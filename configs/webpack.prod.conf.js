const merge = require('webpack-merge');
const config = require('./webpack.config');

const developmentBuildConfig = merge(config, {
    mode: 'production',
    //plugins: []
});

module.exports = new Promise((resolve, reject) => {
    resolve(developmentBuildConfig);
});