const webpack = require('webpack');
const merge = require('webpack-merge');
const config = require('./webpack.config');

const developmentBuildConfig = merge(config, {
    mode: 'development',

    devtool: 'cheap-module-eval-source-map',

    plugins: [
        new webpack.SourceMapDevToolPlugin({
            filename: '[file].map'
        })
    ],

    devServer: {
        contentBase: config.externals.paths.build,
        port: 8081,
        overlay: true
    },
});

module.exports = new Promise((resolve, reject) => {
    resolve(developmentBuildConfig);
});